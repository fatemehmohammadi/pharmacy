package com.example.pharmacy.employee.persistence;

import com.example.pharmacy.Config.RunConfigoration;
import com.example.pharmacy.form.persistence.Form;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "employee" , schema = RunConfigoration.DB)
@Data
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "personel_code" , unique = true)
    private Integer personelCode;
    @Column(name = "national_code" , unique = true)
    private Integer nationalCode;
    @Column(name = "user_name" , nullable = false , unique = true , length = 20)
    private String userName;
    @Column(name = "password" , nullable = false , length = 10)
    @JsonIgnore
    private String password;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email" ,unique = true)
    private String email;
    @Column(name = "phone_number")
    private String phoneNumber;
//    @Column(name = "working_hour")
//    private String workingHour;

    @OneToMany(mappedBy = "employee" , fetch = FetchType.LAZY)
    Collection<Form> forms;
}
