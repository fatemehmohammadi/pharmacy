package com.example.pharmacy.employee.controller;

import com.example.pharmacy.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private EmployeeService employeeService;
    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping(value = "/creat")
    public EmployeeModel create(@RequestBody EmployeeCreatModel employeeCreatModel){
        return employeeService.creat(employeeCreatModel);
    }

    @GetMapping(value = {"","/"})
    public List<EmployeeModel> read(){
        return employeeService.read();
    }

    @PutMapping(value = "/update")
    public EmployeeModel update(@RequestBody EmployeeUpdateModel employeeUpdateModel){
        return employeeService.update(employeeUpdateModel);
    }

    @PutMapping(value = "/employee/form")
    public EmployeeFormUpdateModel updateFormByEmployee(@RequestBody EmployeeFormUpdateModel employeeFormUpdateModel){
        return employeeService.updateFormByEmployee(employeeFormUpdateModel);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody Integer id){
        employeeService.delete(id);
    }


}
