package com.example.pharmacy.employee.controller;

import lombok.Data;

import java.util.Date;

@Data
public class EmployeeFormUpdateModel {
    private Integer id;
    private String body;
    private String type;
    private Boolean status;
    private Date updateAt;
    private Date createdAt;
}
