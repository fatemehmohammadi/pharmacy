package com.example.pharmacy.employee.controller;

import com.example.pharmacy.form.persistence.Form;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class EmployeeUpdateModel {
    private Integer id;
    private String userName;
    private String password;
    private String email;
    private String phoneNumber;

    private List<EmployeeFormModel> employeeFormModels;
}
