package com.example.pharmacy.employee.controller;

import lombok.Data;
import lombok.Getter;

import java.util.Date;

@Data
@Getter
public class EmployeeFormModel {
    private Integer id;
    private String body;
    private String type;
    private Boolean status;
    private Date updateAt;
    private Date createdAt;
}
