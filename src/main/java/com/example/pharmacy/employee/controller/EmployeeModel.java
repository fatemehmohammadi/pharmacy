package com.example.pharmacy.employee.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EmployeeModel {
    private Integer id;
    private Integer nationalCode;
    private Integer personelCode;
//    private String userName;
//    @JsonIgnore
//    private String password;
    private String name;
    private String lastName;
    private String email;
    private String phoneNumber;

    List<EmployeeFormModel> employeeFormModels = new ArrayList<>();
}
