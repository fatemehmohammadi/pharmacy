package com.example.pharmacy.employee.service;

import com.example.pharmacy.employee.controller.*;
import com.example.pharmacy.employee.persistence.Employee;
import com.example.pharmacy.employee.persistence.EmployeeRepository;
import com.example.pharmacy.form.controller.FormModel;
import com.example.pharmacy.form.persistence.Form;
import com.example.pharmacy.form.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository employeeRepository;
    private FormService formService;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, FormService formService) {
        this.employeeRepository = employeeRepository;
        this.formService = formService;
    }


    @Override
    public EmployeeModel creat(EmployeeCreatModel employeeCreatModel) {

        Employee employee = new Employee();
        employee.setEmail(employeeCreatModel.getEmail());
        employee.setPhoneNumber(employeeCreatModel.getPhoneNumber());
        employee.setPersonelCode(employeeCreatModel.getPersonelCode());
        employee.setUserName(employeeCreatModel.getUserName());
        employee.setNationalCode(employeeCreatModel.getNationalCode());
        employee.setPassword(employeeCreatModel.getPassword());
        employee.setLastName(employeeCreatModel.getLastName());
        employee.setName(employeeCreatModel.getName());
        List<FormModel> formModels = new ArrayList<>();
        employeeCreatModel.getEmployeeFormModels().forEach(Value ->{
            formModels.add(new FormModel(
                    null, Value.getType(), Value.getBody(), Value.getStatus(), employee.getId()
            ));
        });

        formService.creat(formModels);
        employeeRepository.save(employee);
        return converttoDto(employee);
    }

    @Override
    public List<EmployeeModel> read() {
        return employeeRepository.findAll()
                .stream()
                .map(EmployeeServiceImpl::converttoDto)
                .collect(Collectors.toList());
    }

    @Override
    public EmployeeModel update(EmployeeUpdateModel employeeUpdateModel) {
        Employee employee = employeeRepository.findById(employeeUpdateModel.getId()).orElseThrow();
        employee.setPhoneNumber(employeeUpdateModel.getPhoneNumber());
        employee.setEmail(employeeUpdateModel.getEmail());
//        try{
        employee.setForms(employeeUpdateModel.getEmployeeFormModels().stream()
        .map(EmployeeServiceImpl::convertToFormEntity)
        .collect(Collectors.toSet()));
//        }catch (Exception e){
//            throw new IllegalStateException("Error not found ", e);
//        }
        return converttoDto(employee);
    }

    @Override
    public EmployeeFormUpdateModel updateFormByEmployee(EmployeeFormUpdateModel employeeFormUpdateModel){
        EmployeeFormUpdateModel newEmployeeFormUpdateModel = new EmployeeFormUpdateModel();
        newEmployeeFormUpdateModel.setBody(employeeFormUpdateModel.getBody());
        newEmployeeFormUpdateModel.setType(employeeFormUpdateModel.getType());
        newEmployeeFormUpdateModel.setUpdateAt(employeeFormUpdateModel.getUpdateAt());
        return newEmployeeFormUpdateModel;
    }

    @Override
    public void delete(Integer id) {
        employeeRepository.deleteById(id);
    }

    public static EmployeeModel converttoDto(Employee employee) {
        EmployeeModel employeeModel = new EmployeeModel();
        employeeModel.setId(employeeModel.getId());
        employeeModel.setEmail(employee.getEmail());
        employeeModel.setName(employee.getName());
        employeeModel.setLastName(employee.getLastName());
        employeeModel.setNationalCode(employee.getNationalCode());
        employeeModel.setPersonelCode(employee.getPersonelCode());
        employeeModel.setPhoneNumber(employee.getPhoneNumber());
        if (employee.getForms() != null) {
            employee.getForms()
                    .stream()
                    .map(EmployeeServiceImpl::convertEmployeeFormToDto)
                    .collect(Collectors.toList());
        }
        return employeeModel;
    }

    public static EmployeeFormModel convertEmployeeFormToDto(Form form) {
        EmployeeFormModel employeeFormModel = new EmployeeFormModel();
        employeeFormModel.setBody(form.getBody());
        employeeFormModel.setType(form.getType());
        employeeFormModel.setCreatedAt(form.getCreatedAt());
        employeeFormModel.setUpdateAt(form.getUpdateAt());
        employeeFormModel.setId(form.getId());
        employeeFormModel.setStatus(form.getStatus());
        return employeeFormModel;
    }

    public static Employee convertToEmployeeEntity(EmployeeModel employeeModel) {
        Employee employee = new Employee();
        employee.setName(employeeModel.getName());
        employee.setLastName(employeeModel.getLastName());
        employee.setEmail(employeeModel.getEmail());
        employee.setNationalCode(employeeModel.getNationalCode());
        employee.setPersonelCode(employeeModel.getPersonelCode());
//        employee.setPassword(employeeModel.getPassword());
//        employee.setUserName(employeeModel.getUserName());
        employee.setPhoneNumber(employeeModel.getPhoneNumber());
        employee.setId(employeeModel.getId());
        employeeModel.getEmployeeFormModels()
                    .stream()
                    .map(EmployeeServiceImpl::convertToFormEntity)
                    .collect(Collectors.toList());
        return employee;
    }

    public static Form convertToFormEntity(EmployeeFormModel employeeFormModel) {
        Form form = new Form();
        form.setId(employeeFormModel.getId());
        form.setType(employeeFormModel.getType());
        form.setStatus(employeeFormModel.getStatus());
        form.setBody(employeeFormModel.getBody());
        form.setCreatedAt(employeeFormModel.getCreatedAt());
        form.setUpdateAt(employeeFormModel.getUpdateAt() );
        return form;
    }
}
