package com.example.pharmacy.employee.service;

import com.example.pharmacy.employee.controller.EmployeeCreatModel;
import com.example.pharmacy.employee.controller.EmployeeFormUpdateModel;
import com.example.pharmacy.employee.controller.EmployeeModel;
import com.example.pharmacy.employee.controller.EmployeeUpdateModel;

import java.util.List;

public interface EmployeeService {
    public EmployeeModel creat(EmployeeCreatModel employeeCreatModel);
    public List<EmployeeModel> read();
    public EmployeeModel update(EmployeeUpdateModel employeeUpdateModel);
    public EmployeeFormUpdateModel updateFormByEmployee(EmployeeFormUpdateModel employeeFormUpdateModel);
    public void delete(Integer id);
}
