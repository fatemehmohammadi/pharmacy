package com.example.pharmacy.form.service;

import com.example.pharmacy.form.controller.FormModel;
import com.example.pharmacy.form.controller.FormUpdateModel;
import com.example.pharmacy.form.persistence.Form;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface FormService {
    public FormModel creat(FormModel formModel);
    public List<FormModel> creat(List<FormModel> formModels);
    public List<FormModel> read();
    public Page<Form> showPage(Pageable pageable);
    public List<FormModel> readByEmployee();
    public List<FormModel> readByManager();
    public List<Form> searchByType(Form form);
    public Set<Form> getListForm(List<Integer> forms_id);
    public FormModel show(Integer id);
//    public List<FormModel> update();
    public void delete(Integer id);
}
