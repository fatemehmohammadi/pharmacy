package com.example.pharmacy.form.service;

import com.example.pharmacy.Category.controller.CategoryModel;
import com.example.pharmacy.Category.persistence.Category;
import com.example.pharmacy.employee.persistence.Employee;
import com.example.pharmacy.form.controller.*;
import com.example.pharmacy.form.persistence.Form;
import com.example.pharmacy.form.persistence.FormRepository;
import com.example.pharmacy.manager.persistence.Manager;
import com.example.pharmacy.manager.persistence.ManagerRepository;
import com.example.pharmacy.manager.service.ManagerServiceImpl;
import com.mysql.cj.x.protobuf.MysqlxExpr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.core.JdbcAggregateOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.RollbackException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class FormServiceImpl implements FormService{

    private FormRepository formRepository;
    private ManagerRepository managerRepository;

    @Autowired
    public FormServiceImpl(FormRepository formRepository, ManagerRepository managerRepository) {
        this.formRepository = formRepository;
    }

    @Override
    public FormModel creat(FormModel formModel) {
        return convertToDto(formRepository.save(convertFormToEntity(formModel)));
    }

    @Override
    public List<FormModel> creat(List<FormModel> formModels) {
        formRepository.saveAll(
                formModels.stream()
                .map(FormServiceImpl::convertFormToEntity)
                .collect(Collectors.toList())
        );
        return formModels;
    }

    @Override
    public List<FormModel> read() {
        return formRepository.findAll().stream()
                .map(FormServiceImpl::convertToDto)
                .collect(Collectors.toList());
    }


    @Override
    public Page<Form> showPage(Pageable pageable) {
        return formRepository.findAll(pageable);
    }

    @Override
    public List<FormModel> readByEmployee() {
        return formRepository.findAll().stream()
                .map(FormServiceImpl::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<FormModel> readByManager() {
        return formRepository.findAll().stream()
                .map(FormServiceImpl::convertToDto)
                .collect(Collectors.toList());
    }
    @Override
    public List<Form> searchByType(Form form){
        return formRepository.findBySearch(form);
    }

    @Override
    public Set<Form> getListForm(List<Integer> forms_id) {
        return new HashSet<>(formRepository.findAllById(forms_id));
    }

    @Override
    public FormModel show(Integer id) {
        return convertToDto(getForm(id));
    }

//    @Override
//    public List<FormModel> update() {
//        return formRepository.findAll()
//                .stream()
//                .map(FormServiceImpl::convertToDto)
//                .collect(Collectors.toList());
//    }

    @Override
    public void delete(Integer id) {
        formRepository.deleteById(id);
    }

    public static FormModel convertToDto(Form form){
        FormModel formModel = new FormModel();
        formModel.setBody(form.getBody());
        formModel.setId(form.getId());
        formModel.setType(form.getType());
        formModel.setCreatedAt(form.getCreatedAt());
        formModel.setUpdateAt(form.getUpdateAt());
        formModel.setStatus(form.getStatus());
        if (form.getEmployee() != null){
            formModel.setEmployee_id(form.getEmployee().getId());
        }
        if (form.getCategory() != null){
            formModel.setCategory_id(form.getCategory().getId());
        }
        formModel.setFormManagerModels(form.getManagerCollection().stream()
        .map(FormServiceImpl::convertFormManagerToDto)
        .collect(Collectors.toList()));
        return formModel;
    }

    public static Form convertFormToEntity(FormModel formModel){
        Form form = new Form();
        form.setId(formModel.getId());
        form.setBody(formModel.getBody());
        form.setStatus(formModel.getStatus());
        form.setType(formModel.getType());
        Category category = new Category();
        category.setId(formModel.getCategory_id());
        form.setCategory(category);
        Employee employee = new Employee();
        employee.setId(formModel.getEmployee_id());
        form.setEmployee(employee);
        form.setManagerCollection(formModel.getFormManagerModels()
        .stream()
                .map(FormServiceImpl::convertFormManagerToEntity)
                .collect(Collectors.toList()));
        return form;
    }

    public  Form getForm(Integer id){
         return formRepository
                .findById(id).orElseThrow(() -> new RollbackException("not found"));
    }

    public static FormEmployeeModel convertFormEmployeeToDto(Employee employee){   //employee
        FormEmployeeModel formEmployeeModel = new FormEmployeeModel();
        formEmployeeModel.setPersonelCode(employee.getPersonelCode());
        return formEmployeeModel;
    }

    public static Employee convertFormEmployeeToEntity(FormEmployeeModel formEmployeeModel){   //employee
        Employee employee = new Employee();
        employee.setPersonelCode(formEmployeeModel.getPersonelCode());
        return employee;
    }

    public static Category convertFormCategoryToEntity(FormCategoryModel formcategoryModel){   //category
        Category category = new Category();
        category.setId(formcategoryModel.getId());
        category.setType(formcategoryModel.getType());
        return category;
    }

    public static FormCategoryModel convertFormCategoryToDto(Category category){         //category
        FormCategoryModel formCategoryModel = new FormCategoryModel();
        formCategoryModel.setId(category.getId());
        formCategoryModel.setType(category.getType());
        return formCategoryModel;
    }

    public static Manager convertFormManagerToEntity(FormManagerModel formManagerModel){   //manager
        Manager manager = new Manager();
        manager.setId(formManagerModel.getId());
        return manager;
    }

    public static FormManagerModel convertFormManagerToDto(Manager manager){              //manager
        FormManagerModel formManagerModel = new FormManagerModel();
        formManagerModel.setId(manager.getId());
        return formManagerModel;
    }
}
