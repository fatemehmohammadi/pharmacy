package com.example.pharmacy.form.persistence;

import com.example.pharmacy.form.controller.FormManagerUpdateModel;
import com.example.pharmacy.manager.controller.ManagerFormUpdateModel;
import com.example.pharmacy.manager.persistence.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormRepository extends JpaRepository<Form, Integer> {

    @Query("select form from Form form WHERE :#{#form.type} is null or form.type like concat('%', :#{#form.type}, '%') ")
    List<Form> findBySearch(Form form);
}
