package com.example.pharmacy.form.persistence;

import com.example.pharmacy.Config.RunConfigoration;
import com.example.pharmacy.employee.persistence.Employee;
import com.example.pharmacy.Category.persistence.Category;
import com.example.pharmacy.manager.persistence.Manager;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Entity
@Table(name = "form" , schema = RunConfigoration.DB)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "type")
    private String type;
    @Column(name = "created_at", updatable = false)
    @CreationTimestamp
    private Date createdAt;
    @Column(name = "update_at")
    @UpdateTimestamp
    private Date updateAt;
    @Column(name = "body")
    private String body;
    @Column(name = "status")
    private Boolean status = false;

//    public Form(String body, String type, Date createdAt, Date updateAt, Boolean status, Employee employees) {
//    }

    @ManyToMany(mappedBy = "formCollection", fetch = FetchType.LAZY)
    Collection<Manager> managerCollection;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private Employee employee;


//    public Form(String body, String type, Date createdAt, Date updateAt, Boolean status, Integer employee_id) {
//    }
}



