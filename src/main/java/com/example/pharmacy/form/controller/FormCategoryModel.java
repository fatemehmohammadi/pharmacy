package com.example.pharmacy.form.controller;

import lombok.Data;

@Data
public class FormCategoryModel {
    private Integer id;
    private String type;
}
