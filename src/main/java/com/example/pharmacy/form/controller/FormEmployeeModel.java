package com.example.pharmacy.form.controller;

import lombok.Data;

@Data
public class FormEmployeeModel {
    private Integer personelCode;
    private Integer id;
}
