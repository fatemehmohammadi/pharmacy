package com.example.pharmacy.form.controller;

import com.example.pharmacy.form.persistence.Form;
import com.example.pharmacy.form.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/form")
public class FormController {

    private FormService formService;

    @Autowired
    public FormController(FormService formService) {
        this.formService = formService;
    }

    @PostMapping({"/"})
    public FormModel creat(@RequestBody FormModel formModel){return formService.creat(formModel);}

    @PostMapping("")
    public List<FormModel> creat(@RequestBody List<FormModel> formModels) { return formService.creat(formModels); }

    @GetMapping(value = {"", "/"})
    public List<FormModel> read() {
        return formService.read();
    }

    @GetMapping(value = "/list form")
    public Set<Form> getListForm(@RequestBody List<Integer> forms_id){ return formService.getListForm(forms_id); }

    @GetMapping(value = {"/id"})
    public FormModel show(@RequestBody Integer id) { return formService.show(id);}

    @GetMapping(value = {"/show page"})
    public Page<Form> showPage(@PageableDefault(size = 5)Pageable pageable) { return formService.showPage(pageable);}


    @DeleteMapping(value = "/")
    public void delete(@RequestBody Integer id) {
        formService.delete(id);
    }
}