package com.example.pharmacy.form.controller;

import com.example.pharmacy.manager.controller.ManagerFormModel;
import com.example.pharmacy.manager.controller.ManagerModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FormModel {
    Integer id;
    String type;
    String body;
    Date createdAt;
    Date updateAt;
    Boolean status;
    Integer category_id;
    Integer employee_id;
    FormEmployeeModel formEmployeeModel;
    FormCategoryModel formCategoryModel;
    List<FormManagerModel> FormManagerModels;

    public FormModel(Object o, String type, String body, Boolean status, Integer id) {
    }
}

