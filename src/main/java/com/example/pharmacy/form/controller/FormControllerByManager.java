package com.example.pharmacy.form.controller;

import com.example.pharmacy.form.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/form/manager")
public class FormControllerByManager {

    private FormService formService;

    @Autowired
    public FormControllerByManager(FormService formService) {
        this.formService = formService;
    }

    @GetMapping(value = "/read")
    public List<FormModel> readByManager(){
        return formService.readByEmployee();
    }
}
