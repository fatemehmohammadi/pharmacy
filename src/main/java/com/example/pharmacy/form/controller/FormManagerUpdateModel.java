package com.example.pharmacy.form.controller;

import com.example.pharmacy.manager.controller.ManagerFormUpdateModel;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FormManagerUpdateModel {
    private String body;
    private String type;
    private Date createdAt;
    private Date updateAt;
    private Boolean status;
    private Integer employee_id;

    private List<FormManagerUpdateModel> formManagerUpdateModels;
}
