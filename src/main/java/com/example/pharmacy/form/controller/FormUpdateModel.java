package com.example.pharmacy.form.controller;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FormUpdateModel {
    private Integer id;
    private String body;
    private Boolean status;
    private String type;
    private Date updateAt;
    Integer category_id;
    Integer employee_id;
    FormEmployeeModel formEmployeeModel;
    FormCategoryModel formCategoryModel;
    List<Integer> FormManagerModels;
}
