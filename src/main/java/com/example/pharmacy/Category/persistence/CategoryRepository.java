package com.example.pharmacy.Category.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

//    Category deleteByType(String type);

    List<Category> findAllByParentId(Integer parent_id);
}
