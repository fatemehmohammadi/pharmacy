package com.example.pharmacy.Category.persistence;

import com.example.pharmacy.Config.RunConfigoration;
import com.example.pharmacy.form.persistence.Form;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Table(name = "category" , schema = RunConfigoration.DB)
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "type")
    private String type;
//    @Column(name = "created_at" , updatable = false)
//    @CreationTimestamp
//    private Date createdAt;
//    @Column(name = "update_at")
//    @UpdateTimestamp
//    private Date updateAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id" , referencedColumnName = "id")
    private Category parent;

    @OneToMany(mappedBy = "category" , fetch = FetchType.LAZY)
    Collection<Form> forms;

    @OneToMany(mappedBy = "parent",fetch =FetchType.LAZY)
    private Collection<Category> child;

}
