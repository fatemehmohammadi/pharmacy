package com.example.pharmacy.Category.service;

import com.example.pharmacy.Category.controller.*;
import com.example.pharmacy.Category.persistence.Category;
import com.example.pharmacy.Category.persistence.CategoryRepository;
import com.example.pharmacy.form.controller.FormModel;
import com.example.pharmacy.form.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;
    private FormService formService;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, FormService formService) {
        this.categoryRepository = categoryRepository;
        this.formService = formService;
    }

    @Override
    public CategoryModel creat(CategoryCreatModel categoryCreatModel) {
        Category category = new Category();
        category.setType(categoryCreatModel.getType());
        if (category.getParent() != null) {
            category.setParent(categoryRepository.getById(categoryCreatModel.getParent().getId()));
        }
//        List<FormModel> formModels = new ArrayList<>();
//        categoryCreatModel.getCategoryFormModels().forEach(Value ->
//                formModels.add(
//                        new FormModel(category.getId(), null)
//                ));
//        formService.creat(formModels);
        return convertToDto(categoryRepository.save(category));
    }

    @Override
    public List<CategoryModel> read() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryServiceImpl::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public CategoryModel update(CategoryUpdateModel categoryUpdateModel) {
        Category category = categoryRepository.findById(categoryUpdateModel.getId()).orElseThrow();
        category.setType(categoryUpdateModel.getType());
        if (categoryUpdateModel.getParent_id() != null) {
            category.setParent(getCategory(categoryUpdateModel.getParent_id()));
        } else {
            category.setParent(null);
        }
        return convertToDto(categoryRepository.save(category));
    }

    @Override
    public void delete(Integer id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public CategoryModel show(Integer id) {
            return convertToDto(getCategory(id));
    }

    public static Category convertToCategoryEntity(CategoryModel categoryModel) {
        Category category = new Category();
        category.setId(categoryModel.getId());
        category.setType(categoryModel.getType());
        if (categoryModel.getParent() != null) {
            category.setParent(category.getParent());
        }
        return category;
    }

    public static CategoryModel convertToDto(Category category) {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setType(category.getType());
        categoryModel.setId(categoryModel.getId());
        if (category.getParent() != null) {
            categoryModel.setParent(convertParentToDto(category.getParent()));
        }
        if (category.getChild() != null) {
            categoryModel.setChild(category.getChild().stream()
                    .map(CategoryServiceImpl::convertChildToDto)
                    .collect(Collectors.toList()));
        }
        return categoryModel;
    }

    public Category getCategory(Integer id) {
        return categoryRepository.findById(id).orElseThrow();
    }


    public static CategoryParentModel convertParentToDto(Category category) {
        CategoryParentModel categoryParentModel = new CategoryParentModel();
        categoryParentModel.setId(category.getId());
        categoryParentModel.setType(category.getType());
        return categoryParentModel;
    }


    public static CategoryChildModel convertChildToDto(Category category) {
        return new CategoryChildModel(category.getId(), category.getType());
    }

    public static CategoryChildTreeModel convertChildTreeToDto(Category category) {
        CategoryChildTreeModel categoryChildTreeModel = new CategoryChildTreeModel();
        categoryChildTreeModel.setType(category.getType());
        categoryChildTreeModel.setId(category.getId());
        categoryChildTreeModel.setParent_id(category.getParent().getId());
        return categoryChildTreeModel;
    }

    @Override
    public List<CategoryChildTreeModel> tree() {
        this.generateTree(null,new CategoryChildTreeModel());
        return categoryRepository.findAllByParentId(null).stream()
                .map(CategoryServiceImpl::convertChildTreeToDto)
                .collect(Collectors.toList());
    }

    public CategoryChildTreeModel generateTree(Integer parentId, CategoryChildTreeModel model) {
        categoryRepository.findAllByParentId(parentId).forEach(value -> {
                    model.setId(value.getId());
                    model.setType(value.getType());
                    generateTree(value.getId(), model);
                }
        );
        return model;
    }
}
