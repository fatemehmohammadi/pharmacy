package com.example.pharmacy.Category.service;

import com.example.pharmacy.Category.controller.CategoryChildTreeModel;
import com.example.pharmacy.Category.controller.CategoryCreatModel;
import com.example.pharmacy.Category.controller.CategoryModel;
import com.example.pharmacy.Category.controller.CategoryUpdateModel;

import java.util.List;

public interface CategoryService {
    public CategoryModel creat(CategoryCreatModel categoryCreatModel);
    public List<CategoryModel> read();
    public CategoryModel update(CategoryUpdateModel categoryUpdateModel);
    public void delete(Integer id);
    public CategoryModel show(Integer id);
    public List<CategoryChildTreeModel> tree();
}
