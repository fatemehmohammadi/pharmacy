package com.example.pharmacy.Category.controller;

import com.example.pharmacy.Category.persistence.Category;
import com.example.pharmacy.employee.controller.EmployeeFormModel;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class CategoryModel {
    private Integer id;
    private String type;
    private CategoryParentModel parent;
    private List<CategoryChildModel> child;
}
