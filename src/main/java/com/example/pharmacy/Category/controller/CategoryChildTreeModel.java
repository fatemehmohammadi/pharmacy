package com.example.pharmacy.Category.controller;

import lombok.Data;

import java.util.List;

@Data
public class CategoryChildTreeModel {
    private String type;
    private Integer id;
    private Integer parent_id;

    private List<CategoryChildTreeModel> categoryChildTreeModels;
}
