package com.example.pharmacy.Category.controller;

import com.sun.istack.Interned;
import lombok.Data;
import org.springframework.http.converter.json.GsonBuilderUtils;

@Data
public class CategoryUpdateModel {
    private Integer id;
    private String type;
    private Integer parent_id;
}
