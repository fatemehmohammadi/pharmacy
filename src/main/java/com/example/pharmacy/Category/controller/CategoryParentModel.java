package com.example.pharmacy.Category.controller;

import lombok.Data;

@Data
public class CategoryParentModel {
    private String type;
    private Integer id;
}
