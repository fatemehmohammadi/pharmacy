package com.example.pharmacy.Category.controller;

import com.example.pharmacy.Category.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {
    CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @PostMapping(value = "/creat")
    public CategoryModel creat(@RequestBody CategoryCreatModel categoryCreatModel){
        return categoryService.creat(categoryCreatModel);
        }

    @GetMapping(value = {"/",""})
    public List<CategoryModel> read(){
        return categoryService.read();
            }

    @PutMapping(value = "/update")
    public CategoryModel update(@RequestBody CategoryUpdateModel categoryUpdateModel){
        return categoryService.update(categoryUpdateModel);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody Integer id){
        categoryService.delete(id);
    }

    @GetMapping(value = {"/show"})
    public CategoryModel show(@RequestBody Integer id) {
        return categoryService.show(id);
    }

    @GetMapping(value = "/tree")
    public List<CategoryChildTreeModel> tree() {return categoryService.tree(); }
}
