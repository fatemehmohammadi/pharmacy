package com.example.pharmacy.Category.controller;

import com.example.pharmacy.Category.persistence.Category;
import lombok.Data;

@Data
public class CategoryChildModel {
    private String type;
    private Integer id;
    private Integer parent_id;

    public CategoryChildModel(Integer id, String type) {
    }
}

