package com.example.pharmacy.Category.controller;

import com.example.pharmacy.Category.persistence.Category;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CategoryCreatModel {

    private String type;
    private Category parent;

    List<CategoryFormModel> categoryFormModels = new ArrayList<>();
}
