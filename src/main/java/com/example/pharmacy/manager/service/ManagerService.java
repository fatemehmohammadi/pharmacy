package com.example.pharmacy.manager.service;

import com.example.pharmacy.form.persistence.Form;
import com.example.pharmacy.manager.controller.ManagerFormUpdateModel;
import com.example.pharmacy.manager.controller.ManagerModel;
import com.example.pharmacy.manager.controller.ManagerUpdateModel;

import java.util.List;
import java.util.Set;

public interface ManagerService {
    public ManagerModel creat(ManagerModel managerModel);
    public List<ManagerModel> read();
    public ManagerModel update(ManagerUpdateModel managerUpdateModel);
    public ManagerFormUpdateModel formUpdateByManager(ManagerFormUpdateModel managerFormUpdateModel);
    public void delete(Integer id);
}
