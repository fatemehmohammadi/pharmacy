package com.example.pharmacy.manager.service;

import com.example.pharmacy.form.persistence.Form;
import com.example.pharmacy.form.service.FormService;
import com.example.pharmacy.manager.controller.*;
import com.example.pharmacy.manager.persistence.Manager;
import com.example.pharmacy.manager.persistence.ManagerJtForm;
import com.example.pharmacy.manager.persistence.ManagerJtFormRepository;
import com.example.pharmacy.manager.persistence.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class ManagerServiceImpl implements ManagerService{

    private ManagerRepository managerRepository;
    private FormService formService;
    private ManagerJtFormRepository managerJtFormRepository;

    @Autowired
    public ManagerServiceImpl(ManagerRepository managerRepository, FormService formService, ManagerJtFormRepository managerJtFormRepository) {
        this.managerRepository = managerRepository;
        this.formService = formService;
        this.managerJtFormRepository = managerJtFormRepository;
    }

    @Override
    public ManagerModel creat(ManagerModel managerModel) {
        return convertToDto(managerRepository.save(convertManagerToEntity(managerModel)));
    }

    @Override
    public List<ManagerModel> read() {
        return managerRepository.findAll()
                .stream()
                .map(ManagerServiceImpl::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ManagerModel update(ManagerUpdateModel managerUpdateModel) {
        Manager manager = managerRepository.findById(managerUpdateModel.getId()).orElseThrow();
        manager.setEmail(managerUpdateModel.getEmail());
        manager.setPhoneNumber(managerUpdateModel.getPhoneNumber());
        Set<Form> forms = formService.getListForm(managerUpdateModel.getFormId());
        manager.setFormCollection(forms);
        return convertToDto(manager);
    }

    @Override
    public ManagerFormUpdateModel formUpdateByManager(ManagerFormUpdateModel managerFormUpdateModel){

        ManagerFormUpdateModel newManagerFormUpdateModel = new ManagerFormUpdateModel();
        List<Manager> managerList = managerRepository.findAll();
        List<ManagerJtForm> managerJtForms = managerJtFormRepository.findByIsConfirm(managerFormUpdateModel).stream()
                .collect(Collectors.toList());
        if (managerList.size() == managerJtForms.size()){
            managerFormUpdateModel.setStatus(true);
        }
        return managerFormUpdateModel;
    }

    @Override
    public void delete(Integer id) {
//        try {
            managerRepository.deleteById(id);
//        } catch (Exception e) {
//            e.notify();
//        }
    }

    public static ManagerModel convertToDto(Manager manager){
        ManagerModel managerModel = new ManagerModel();
        managerModel.setPassword(manager.getPassword());
        managerModel.setUserName(manager.getUserName());
        managerModel.setEmail(manager.getEmail());
        managerModel.setId(manager.getId());
        managerModel.setLastName(manager.getLastName());
        managerModel.setName(manager.getName());
        managerModel.setPersonelCode(manager.getPersonelCode());
        managerModel.setNationalCode(manager.getNationalCode());
        if (manager.getFormCollection() != null){
            manager.getFormCollection()
                    .stream()
                    .map(ManagerServiceImpl::convertManagerFormToDto)
                    .collect(Collectors.toList());
        }
        return managerModel;
    }

    public static Manager convertManagerToEntity(ManagerModel managerModel){
        Manager manager = new Manager();
        manager.setPhoneNumber(managerModel.getPhoneNumber());
        manager.setEmail(managerModel.getEmail());
        manager.setId(managerModel.getId());
        manager.setName(managerModel.getName());
        manager.setLastName(managerModel.getLastName());
        manager.setNationalCode(managerModel.getNationalCode());
        manager.setPersonelCode(managerModel.getPersonelCode());
        manager.setUserName(managerModel.getUserName());
        manager.setPassword(managerModel.getPassword());
        if (managerModel.getManagerFormModels() != null) {
            manager.setFormCollection(managerModel.getManagerFormModels().stream()
                    .map(ManagerServiceImpl::convertManagerFormToEntity)
                    .collect(Collectors.toList()));
        }
        return manager;
    }

    public static ManagerFormModel convertManagerFormToDto(Form form){
        ManagerFormModel managerFormModel = new ManagerFormModel();
        managerFormModel.setBody(form.getBody());
        managerFormModel.setType(form.getType());
        managerFormModel.setId(form.getId());
        managerFormModel.setEmployee_id(form.getEmployee());
        return managerFormModel;
    }

    public ManagerModel convertManagerUpdateModelToFormModel(ManagerUpdateModel managerUpdateModel){
        return new ManagerModel(managerUpdateModel.getId(), managerUpdateModel.getEmail(), managerUpdateModel.getFormId(),managerUpdateModel.getPhoneNumber());
    }


    public static Form convertManagerFormToEntity(ManagerFormModel managerFormModel){
        Form form = new Form();
        form.setUpdateAt(managerFormModel.getUpdateAt());
        form.setCreatedAt(managerFormModel.getCreatedAt());
        form.setStatus(managerFormModel.getStatus());
        form.setBody(managerFormModel.getBody());
        form.setType(managerFormModel.getType());
        form.setId(managerFormModel.getId());
        form.setEmployee(managerFormModel.getEmployee_id());
        return form;
    }
}