package com.example.pharmacy.manager.controller;

import com.example.pharmacy.form.controller.FormManagerModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class ManagerCreatModel {
    private String userName;
    @JsonIgnore
    private String password;
    private String name;
    private String lastName;
    private Integer personelCode;
    private Integer nationalCode;
    private String email;
    private String phoneNumber;
//    private String workingHour;

    private List<FormManagerModel> formManagerModels;
}
