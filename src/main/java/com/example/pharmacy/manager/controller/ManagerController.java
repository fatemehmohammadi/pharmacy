package com.example.pharmacy.manager.controller;

import com.example.pharmacy.manager.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manager")
public class ManagerController {

    private ManagerService managerService;
    @Autowired
    public ManagerController(ManagerService managerService) {
        this.managerService = managerService;
    }

    @PostMapping("/creat")
    public ManagerModel creat(@RequestBody ManagerModel managerModel){ return managerService.creat(managerModel); }

    @GetMapping(value = {"","/"})
    public List<ManagerModel> read(){
        return managerService.read();
    }

    @PutMapping(value = "/update")
    public ManagerModel update(@RequestBody ManagerUpdateModel managerUpdateModel){
        return managerService.update(managerUpdateModel);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody Integer id){
        managerService.delete(id);
    }

    @PutMapping(value = "/manager/form")
    public ManagerFormUpdateModel formUpdateByManager(@RequestBody ManagerFormUpdateModel managerFormUpdateModel){
        return managerService.formUpdateByManager(managerFormUpdateModel);
    }
}
