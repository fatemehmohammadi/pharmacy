package com.example.pharmacy.manager.controller;

import com.example.pharmacy.employee.persistence.Employee;
import com.example.pharmacy.form.persistence.Form;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Date;

@Data
public class ManagerFormModel {

    private Integer id;
    private String body;
    private String type;
    private Date createdAt;
    private Date updateAt;
    private Boolean status;
    private Employee employee_id;
}
