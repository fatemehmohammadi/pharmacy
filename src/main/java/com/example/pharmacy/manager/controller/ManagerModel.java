package com.example.pharmacy.manager.controller;

import com.example.pharmacy.form.persistence.Form;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import java.util.List;
@Data
public class ManagerModel {
    private Integer id;
    private String userName;
    @JsonIgnore
    private String password;
    private String name;
    private String lastName;
    private Integer personelCode;
    private Integer nationalCode;
    private String email;
    private String phoneNumber;

    private List<ManagerFormModel> managerFormModels;

    public ManagerModel(Integer id, String email, List<Integer> formId, String phoneNumber) {
    }

    public ManagerModel() {

    }
}
