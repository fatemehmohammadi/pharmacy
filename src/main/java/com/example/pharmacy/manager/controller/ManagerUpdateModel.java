package com.example.pharmacy.manager.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;
@Data
public class ManagerUpdateModel {
    private Integer id;
    private String email;
    private String phoneNumber;
//    private List<Integer> formId;
    private List<Integer> formId;
}
