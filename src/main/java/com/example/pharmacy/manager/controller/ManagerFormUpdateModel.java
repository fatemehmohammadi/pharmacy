package com.example.pharmacy.manager.controller;

import com.example.pharmacy.employee.controller.EmployeeModel;
import com.example.pharmacy.form.persistence.Form;
import lombok.Data;

import java.util.Date;
import java.util.List;
@Data
public class ManagerFormUpdateModel {
//    private Integer id;
    private String body;
    private String type;
    private Date createdAt;
    private Date updateAt;
    private Boolean status;
    private Integer employee_id;

    private List<ManagerFormUpdateModel> managerFormUpdateModels;
}
