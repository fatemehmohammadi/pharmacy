package com.example.pharmacy.manager.persistence;

import com.example.pharmacy.manager.controller.ManagerFormUpdateModel;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {
}
