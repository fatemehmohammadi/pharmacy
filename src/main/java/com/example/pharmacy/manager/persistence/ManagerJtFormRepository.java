package com.example.pharmacy.manager.persistence;

import com.example.pharmacy.manager.controller.ManagerFormUpdateModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ManagerJtFormRepository extends JpaRepository<ManagerJtForm, Integer> {

    @Query("select manager from ManagerJtForm manager where manager.isConfirm = true")
    List<ManagerJtForm> findByIsConfirm(ManagerFormUpdateModel managerFormUpdateModel);
}
