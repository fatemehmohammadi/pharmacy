package com.example.pharmacy.manager.persistence;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.MapKey;
import java.io.Serializable;

@Embeddable
@Data
public class ManagerJtFormId implements Serializable {

    private Integer managerId;
    private Integer formId;
}
