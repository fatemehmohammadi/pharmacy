package com.example.pharmacy.manager.persistence;

import com.example.pharmacy.Config.RunConfigoration;
import com.example.pharmacy.form.persistence.Form;
import lombok.Data;

import javax.naming.spi.NamingManager;
import javax.persistence.*;

@Entity
@Table(name = "managerJtForm", schema = RunConfigoration.DB)
@Data
public class ManagerJtForm {

    @EmbeddedId
    private ManagerJtFormId managerJtFormId;

    @ManyToOne
    @MapsId("managerId")
    private Manager manager;

    @ManyToOne
    @MapsId("formId")
    private Form form;

    @Column(name = "is_confirm")
    private Boolean isConfirm = Boolean.FALSE;

}
