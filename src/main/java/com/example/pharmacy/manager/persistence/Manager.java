package com.example.pharmacy.manager.persistence;

import com.example.pharmacy.Config.RunConfigoration;
import com.example.pharmacy.form.persistence.Form;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "manager" , schema = RunConfigoration.DB)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Manager {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "national_code" , unique = true)
    private Integer nationalCode;
    @Column(name = "personel_code" , unique = true)
    private Integer personelCode;
    @Column(name = "user_name" , nullable = false , unique = true , length = 20)
    private String userName;
    @Column(name = "password" , nullable = false , length = 10)
    @JsonIgnore
    private String password;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email" , unique = true)
    private String email;
    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "manager_fk_form",
            joinColumns = {@JoinColumn(name = "manager_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "form_id", referencedColumnName = "id")})
    Collection<Form> formCollection;

}
