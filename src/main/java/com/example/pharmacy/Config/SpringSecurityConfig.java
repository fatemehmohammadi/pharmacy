package com.example.pharmacy.Config;

import org.springframework.boot.autoconfigure.security.ConditionalOnDefaultWebSecurity;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable()
//                .authorizeRequests().antMatchers("/css/**", "/js/**", "/img/**").permitAll()
//                .anyRequest().authenticated()
//                .and().formLogin()
//                .loginPage("/form").permitAll().and().logout().permitAll()
//                .and().formLogin()
//                .loginPage("/employee").permitAll().and().logout().permitAll()
//                .and().formLogin()
//                .loginPage("/manager").permitAll().and().logout().permitAll()
//                .and().formLogin()
//                .loginPage("/category").permitAll().and().logout().permitAll();
    }
}
